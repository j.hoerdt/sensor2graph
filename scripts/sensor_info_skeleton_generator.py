info = [
    (
        ["pressure", "altitude", "pressure_sealevel", "temperature", "humidity"],
        "BME280",
    ),
    (["pressure", "altitude", "pressure_sealevel", "temperature"], "BMP180"),
    (["pressure", "altitude", "pressure_sealevel", "temperature"], "BMP280"),
    (["temperature", "humidity"], "DHT22"),
    (["temperature"], "DS18B20"),
    (["P1", "P2"], "HPM"),
    (["temperature", "humidity"], "HTU21D"),
    (
        ["noise_LAeq", "noise_LA_min", "noise_LA_max", "noise_LA01", "noise_LA95"],
        "Laerm",
    ),
    (["P1", "P2", "P0"], "PMS1003"),
    (["P1", "P2", "P0"], "PMS3003"),
    (["P1", "P2", "P0"], "PMS5003"),
    (["P1", "P2", "P0"], "PMS6003"),
    (["P1", "P2", "P0"], "PMS7003"),
    (["P1", "durP1", "ratioP1", "P2", "durP2", "ratioP2"], "PPD42NS"),
    (
        ["counts_per_minute", "hv_pulses", "tube", "counts", "sample_time_ms"],
        "Radiation_SBM-19",
    ),
    (
        ["counts_per_minute", "hv_pulses", "tube", "counts", "sample_time_ms"],
        "Radiation_SBM-20",
    ),
    (
        ["counts_per_minute", "hv_pulses", "tube", "counts", "sample_time_ms"],
        "Radiation_Si22G",
    ),
    (["P1", "durP1", "ratioP1", "P2", "durP2", "ratioP2"], "SDS011"),
    (["temperature", "humidity"], "SHT11"),
    (["temperature", "humidity"], "SHT30"),
    (["temperature", "humidity"], "SHT31"),
    (["temperature", "humidity"], "SHT35"),
    (["temperature", "humidity"], "SHT85"),
    (["P1", "P4", "P2", "P0", "N10", "N4", "N25", "N1", "N05", "TS"], "SPS30"),
]

import json

print(json.dumps({ "sensor_type_info": [{
    "measured_variables": variables,
    "props": {
        "manufacturers": "asdf",
        "description": "asdf",
        "instrument_type": "asdf",
        "sensor_type": typ
    }
} for variables, typ in info]}))


