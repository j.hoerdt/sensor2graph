#!/usr/bin/env python

import sys
import json
import jsonschema
import urllib.request
import yaml

#pid_to_test = "21.11138/2f92c6f6-891b-46ec-9b16-ca0be3342589"
pid_to_test = "21.11138/a6442237-381e-4e97-92f7-74b7749350da"
#pid_to_test = "21.11138/c4c0f351-96e2-4e93-bf1f-0fd73e828300"

base_url = "https://hdl.handle.net/"

exit_code = 0
pid_content = json.loads(urllib.request.urlopen(f"{base_url}api/handles/{pid_to_test}").read())
for record in pid_content["values"]:
	if record["index"] not in [100,9]:
		try:
			print(f"validating {record['data']['value']} against schema {record['type']}")
			data = json.loads(record["data"]["value"])
			type_info = json.loads(urllib.request.urlopen(base_url + record["type"]).read())

			jsonschema.validate(instance=data, schema=json.loads(type_info["validationSchema"]))
		except Exception as e:
			print(f"could not validate due to exception: {e}")
			exit_code = 1

sys.exit(exit_code);
