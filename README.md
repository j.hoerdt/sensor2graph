# sensor2graph
[![pipeline status](https://gitlab.gwdg.de/j.hoerdt/sensor2graph/badges/master/pipeline.svg)](https://gitlab.gwdg.de/j.hoerdt/sensor2graph/-/commits/master)

This project aims to deliver metadata about the sensors involved in the open [sensor.community](https://sensor.community) project in the form of the graph database [neo4j](https://neo4j.com).

The latest artifacts can be browsed [here](https://gitlab.gwdg.de/j.hoerdt/sensor2graph/-/jobs/artifacts/master/browse?job=buildjarfile).

## usage instructions:
### executing the latest binaries:
#### via docker image:
1. Make sure to have [docker installed](https://docs.docker.com/engine/install/) on your system.
2. Run the program by executing the following command in the command line:

	```
	docker run --rm -d -v sensor2graph_data:/data docker.gitlab.gwdg.de/j.hoerdt/sensor2graph:master <username> <password>
	```

	In the command, replace `<username>` and `<password>` by your username and password for the neo4j database respectively.



#### via jar file:
1. Download the latest artifacts [here](https://gitlab.gwdg.de/j.hoerdt/sensor2graph/-/jobs/artifacts/master/download?job=buildjarfile).
1. Execute

	**using docker:**

	1. Make sure to have [docker installed](https://docs.docker.com/engine/install/) on your system.
	1. Run this command in the root of the artifacts archive:
	
		```
		docker run --rm -u gradle -v "$PWD":/home/gradle/project -w /home/gradle/project gradle:jre14 java -Djava.util.logging.config.file=logging.properties -jar build/libs/sensor2graph.jar <username> <password>
		```

	**using the system's jre:**

	1. <a name="installjdk"></a>Install a java jre on your system if you do not already have one. Java versions 11 and 13 are known to work. Installation instructions for OpenJDK are [here](https://openjdk.java.net/install/).
	1. <a name="runjar"></a>Run the program using the following command from the root of the directory:

		```
		java -Djava.util.logging.config.file=logging.properties -jar build/libs/sensor2graph.jar <username> <password>
		```

		In the command, replace `<username>` and `<password>` by your username and password for the neo4j database respectively.



### building the docker image from source
1. Make sure to have [docker installed](https://docs.docker.com/engine/install/) on your system.
2. In the repository, run the following command:
	```
	docker build -t sensor2graph .
	```
3. The container can be executed with the following command:
	```
	docker run --rm -d -v sensor2graph_data:/data sensor2graph <username> <password>
	```

	In the command, replace `<username>` and `<password>` by your username and password for the neo4j database respectively.


### building the jarfile from source :
#### with docker:
1. If docker is installed on the system you can run

	```
	docker run --rm -u gradle -v "$PWD":/home/gradle/project -w /home/gradle/project gradle:jdk14 gradle jar
	```

#### without docker:
1. To build the project a jdk is needed. Refer to [this point 2.](#installjdk) in "via jar file:" for more.
1. Then, after you have cloned this repository, create a fat jar file by running the following command in the repository's root:

	```
	./gradlew jar
	```
	This is the [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) which automatically installs the correct gradle version into your home directory and creates the jar file.

	The internal api docs can be built with `./gradlew javadoc`.

1. Proceed with [point 2 of via jar file](#runjar).


## Developers:
### Updating dependencies:
Check driver compatibility here:
https://support.neo4j.com/hc/en-us/articles/115013134648-Neo4j-Supported-Versions

Look for new versions here:
- https://search.maven.org/artifact/org.neo4j.driver/neo4j-java-driver
- https://search.maven.org/artifact/com.google.code.gson/gson
- https://search.maven.org/artifact/com.squareup.okhttp3/okhttp
- https://search.maven.org/artifact/com.influxdb/influxdb-client-java

and update the entries in the `build.gradle` file.

### Updating gradle and gradle wrapper
1. Check https://gradle.org/releases/.
2. `gradle wrapper --gradle-version <version>`
3. commit