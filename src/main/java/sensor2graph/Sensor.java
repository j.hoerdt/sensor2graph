package sensor2graph;

import com.google.gson.*;
import okhttp3.*;
import org.neo4j.driver.Record;
import org.neo4j.driver.*;
import sensor2graph.georev.*;

import java.time.*;
import java.util.*;
import java.util.function.*;
import java.util.logging.*;
import java.util.regex.*;
import java.util.stream.*;

class Sensor {
	public Map<String, Object> properties = new HashMap<>();

	public void process(Session session, TransactionConfig transaction_config) throws Exception {
		var rec = session.writeTransaction(tx -> {
			Result res = tx.run(get_creation_query());
			return res.single();
		}, transaction_config);

		String pid = rec.get("s.pid").asString();
		properties.put("node_id", rec.get("id(s)").asObject());

		if (pid.equals("null")) {
			//no pid exists for this sensor
			properties.put("pid", register_new_pid(rec));
			put_pid_in_graphdb(session, transaction_config);
		} else {
			update_pid(pid, rec);
		}
	}

	private void put_pid_in_graphdb(Session session, TransactionConfig transaction_config) {
		Query pid_query = new Query("match (n) where id(n) = $node_id set n.pid = $pid").withParameters(properties);

		session.writeTransaction(tx -> tx.run(pid_query), transaction_config);
	}

	private String register_new_pid(Record rec) throws Exception {
		String response_body = request_to_registry("PUT", Main.config.getProperty("prefix_for_new_pids") + UUID.randomUUID().toString(), rec);
		var created_pid = new Gson().fromJson(response_body, JsonObject.class).get("handle").getAsString();
		Main.glogger.info("created pid: " + created_pid);
		return created_pid;
	}

	private void update_pid(String pid, Record rec) throws Exception {
		request_to_registry("PUT", pid, rec);
	}

	private String request_to_registry(String method, String target_uri, Record rec) throws Exception {
		var body = new JsonObject();
		body.add("values", new JsonArray());

		{
			var admin_value = new JsonObject();
			admin_value.addProperty("index", "100");
			admin_value.addProperty("type", "HS_ADMIN");
			{
				var data = new JsonObject();
				data.addProperty("format", "admin");
				{
					var value = new JsonObject();
					value.addProperty("index", "300");
					value.addProperty("handle", "21.11138/USER01");
					value.addProperty("permissions", "110011111111");
					data.add("value", value);
				}
				admin_value.add("data", data);
			}
			body.getAsJsonArray("values").add(admin_value);
		}


		if (!rec.get("s.pid").isNull()) {
			add_attribute(body,
				"1",
				"21.T11148/8eb858ee0b12e8e463a5",
				"{\"identifier-general-with-type\":{\"identifierValue\":\""+ rec.get("s.pid").asString() +"\", \"identifierType\":\"Handle\"}}"
			);
		}
		add_attribute(body,
			"2",
			"21.T11148/22c62082a4d2d9ae2602",
			"{\"Dates\":[{\"date\":{\"dateValue\":\"" + rec.get("earliest_msg").asLocalDate().toString() + "\",\"dateType\":\"Commissioned\"}},{\"date\":{\"dateValue\":\"" + rec.get("latest_msg").asLocalDate().toString() + "\"}}]}"
		);
		add_attribute(body,
			"3",
			"21.T11148/709a23220f2c3d64d1e1",
			'\"' + rec.get("s.sensor_id").asString() + '\"'
		);
		add_attribute(body,
			"4",
			"21.T11148/c1a0ec5ad347427f25d6",
			"{\"Model\":{\"modelName\":\"" + rec.get("type.sensor_type").asString() + "\", \"modelIdentifier\":{\"modelIdentifierValue\":\"" + rec.get("type.sensor_type").asString() + "\"}}}"
		);
		add_attribute(body,
			"5",
			"21.T11148/9a15a4735d4bda329d80",
			"\"https://sensor.community\""
		);
		add_attribute(body,
			"6",
			"21.T11148/4eaec4bc0f1df68ab2a7",
			"{\"Owners\":[{\"owner\":{\"ownerName\":\"SensorCommunity\",\"ownerContact\":\"contact@open-forecast.eu\"}}]}"
		);
		add_attribute(body,
			"7",
			"21.T11148/1f3e82ddf0697a497432",
			"{\"Manufacturers\":[{\"manufacturer\":{\"manufacturerName\":\""+ rec.get("type.manufacturer").asString() +"\"}}]}"
		);
		if (!rec.get("type.description").isNull()) {
			add_attribute(body,
				"8",
				"21.T11148/55f8ebc805e65b5b71dd",
				new JsonPrimitive(rec.get("type.description").asString()).toString()
			);
		}
		add_attribute(body,
			"9",
			"URL",
			"https://sensor.community/"
		);
		add_attribute(body,
			"10",
			"21.T11148/72928b84e060d491ee41",
			"{\"MeasuredVariables\":[" +
			rec.get("collect(measured_variable.variable)")
				.asList(Values.ofString())
				.stream()
				.map(mv -> "{\"measuredVariable\":\"" + mv + "\"}")
				.collect(Collectors.joining(",")) +
			"]}"
		);
		if (!rec.get("type.classification").isNull()) {
			add_attribute(body,
				"11",
				"21.T11148/f76ad9d0324302fc47dd",
				'\"' + rec.get("type.classification").asString() + '\"'
			);
		}
		add_attribute(body,
			"12",
			"21.T11148/f5e68cc7718a6af2a96c",
			"{\"SchemaVersion\":\"1.0b1\"}"
		);

		Main.glogger.fine("sent body: " + body);

		Request request = new Request.Builder()
			.url(Main.config.getProperty("pid_registry_uri") + target_uri)
			.method(method, RequestBody.create(body.toString(), MediaType.parse("application/json")))
			.header("Accept", "application/json")
			.header("Content-Type", "application/json")
			.header("Authorization", "Handle clientCert=\"true\"")
			.build();

		Main.glogger.fine("request uri: " + Main.config.getProperty("pid_registry_uri") + target_uri);

		try (Response response = Handle.http_client.newCall(request).execute()) {
			var response_body = response.body().string();
			Main.glogger.fine("status: " + response.code());
			Main.glogger.fine("resp: " + response_body);

			if (response.code() / 100 != 2) {
				throw new Exception("request: " + request + " to pid registry failed with status code " + response.code() + ", reponse: " + response_body);
			}
	
			return response_body;
		}
	}

	private void add_attribute(JsonObject body, String index, String type, String data) {
		var obj = new JsonObject();
		obj.addProperty("index", index);
		obj.addProperty("type", type);
		obj.addProperty("data", data);
		body.getAsJsonArray("values").add(obj);
	}

	private Query get_creation_query() {
		BinaryOperator<String> string_if_exists = (dependency, merge_string) -> (properties.get(dependency) != null ? merge_string : "");

		return new Query(
			string_if_exists.apply("country",
				"merge (country:Country {code: $country_code})" + 
				"on create set country.name = $country\n"
			) +
			string_if_exists.apply("state",
				"merge (state:State {name: $state})" +
				string_if_exists.apply("country", "-[:IN_COUNTRY]->(country)")
			) +
			string_if_exists.apply("city",
				"merge " +
				string_if_exists.apply("country", "(country)<-[:IN_COUNTRY]-") +
				"(city:City {name: $city})" +
				string_if_exists.apply("state", "-[:IN_STATE]->(state)")
			) +
			/*
			*/
			"merge (type:SensorType {sensor_type: $sensor_type})" +
			"merge (mobility:Mobility {type: $mobility})" +
			"merge (environment:Environment {type: $environment})" +
			"merge (station:Station {location: $location })" +
			
			//sensor merge here:
			"merge (type)<-[:HAS_TYPE]-(s:Sensor {sensor_id: $sensor_id})-[:BELONGS_TO]->(station)" +

			"merge (s)-[:MEASURED_AT]->(measurement:MeasurementLocation {coordinates: $coordinates})" +
			"merge (s)-[:IS_LOCATED]->(environment)" +
			"merge (s)-[:HAS_MOBILITY]->(mobility)" +
			
			string_if_exists.apply("country", "merge (measurement)-[:IN]->(country)") +
			string_if_exists.apply("state",   "merge (measurement)-[:IN]->(state)") +
			string_if_exists.apply("city",    "merge (measurement)-[:IN]->(city)") +

			"set" +
			"\tmeasurement.first_msg = case when $date > measurement.first_msg then measurement.first_msg else $date end," +
			"\tmeasurement.last_msg  = case when $date < measurement.last_msg  then measurement.last_msg  else $date end " +
			
			"with s, type\n" +
			"match (s)--(all_measurements:MeasurementLocation)" +
			// only one aggregation per with/return
			"with s, type, min(all_measurements.first_msg) as earliest_msg, max(all_measurements.last_msg) as latest_msg\n" +
			"optional match (type)--(measured_variable:MeasuredVariable)" +
			"return s.pid, id(s), s.sensor_id, type.sensor_type, type.manufacturer, type.classification, type.description, earliest_msg, latest_msg, collect(measured_variable.variable)"
		).withParameters(properties);
	}

	private static Predicate<String> contains_indoor_string = Pattern.compile(".+indoor.csv").asMatchPredicate();
	
	public static Sensor fromCSV(String file_uri, Map<String, String> data_as_strings) throws Exception {
		Sensor result = new Sensor();
		result.properties.put("environment", contains_indoor_string.test(file_uri) ? "indoor" : "outdoor");
		result.properties.put("mobility",    "stationary");
		result.properties.put("sensor_id",   data_as_strings.get("sensor_id"));
		result.properties.put("location",    data_as_strings.get("location"));
		result.properties.put("sensor_type", data_as_strings.get("sensor_type"));
		result.properties.put("date",        LocalDate.ofInstant(
			Util.parseAnyDateTime(data_as_strings.get("timestamp")),
			ZoneOffset.UTC
		));

		try {
			add_geographic_data(data_as_strings, result);
		} catch (NumberFormatException e) {
			Main.glogger.log(Level.WARNING, "could not parse coordinates(" + data_as_strings.get("lat") + ", " + data_as_strings.get("lon") + ") from " + file_uri + " because: " + e.getMessage() + ", using (0,0) instead and not attaching a city.");
			result.properties.put("coordinates", Values.point(/*srid for geographic coordinates*/ 4326, 0, 0));
		}

		return result;
	}


	private static void add_geographic_data(Map<String, String> data_as_strings, Sensor result) throws Exception {
		double latitude  = Double.parseDouble(data_as_strings.get("lat"));
		double longitude = Double.parseDouble(data_as_strings.get("lon"));
		result.properties.put("coordinates", Values.point(/*srid for geographic coordinates*/ 4326, longitude, latitude));

		Geodata geodata = ReverseGeocoder.osm_instance().reverse_geocode(latitude, longitude);
		if (geodata.country_code != null) {
			result.properties.put("country_code", geodata.country_code);
			result.properties.put("country", geodata.country);
			result.properties.put("city", geodata.city);
			result.properties.put("state", geodata.state);
		} else {
			Main.glogger.log(Level.WARNING, "country_code is the key of the Country nodes and can't be null, which is what I got from reverse_geocode(" + latitude + ", " + longitude + ") what could not determine the country_code apparently. Not attaching a city consequently.");
		}
	}
}