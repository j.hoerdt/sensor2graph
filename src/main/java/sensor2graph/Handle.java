package sensor2graph;

import okhttp3.*;

import javax.net.ssl.*;
import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.time.*;

public class Handle {
	private Handle() {}

	public static OkHttpClient http_client;

	private static TrustManager[] get_trust_managers() throws Exception {
		var server_self_signed_certificate = CertificateFactory.getInstance("X.509")
				.generateCertificate(new FileInputStream(Main.config.getProperty("pid_reg_server_cert")));

		var server_cert_public_key_store = KeyStore.getInstance(KeyStore.getDefaultType());
		server_cert_public_key_store.load(null, null);
		server_cert_public_key_store.setCertificateEntry("vm13.pid.gwdg.de", server_self_signed_certificate);

		var trust_manager_factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trust_manager_factory.init(server_cert_public_key_store);
		return trust_manager_factory.getTrustManagers();
	}

	private static KeyManager[] get_key_managers() throws Exception {
		var private_key_store = KeyStore.getInstance(KeyStore.getDefaultType());
		private_key_store.load(new FileInputStream(Main.config.getProperty("pid_reg_keystore")), "asdfasdf".toCharArray());
		var key_manager_factory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		key_manager_factory.init(private_key_store, "asdfasdf".toCharArray());
		return key_manager_factory.getKeyManagers();
	}

	private static HostnameVerifier allowAllHostNames() {
		return (hostname, sslSession) -> true;
	}

	public static OkHttpClient get_client() throws Exception {
		var trust_managers = get_trust_managers();
		var trust_self_signed_cert_and_provide_client_cert = SSLContext.getInstance("TLS");
		trust_self_signed_cert_and_provide_client_cert.init(get_key_managers(), trust_managers, null);

		return new OkHttpClient.Builder()
            .sslSocketFactory(trust_self_signed_cert_and_provide_client_cert.getSocketFactory(), (X509TrustManager) trust_managers[0])
            .hostnameVerifier(allowAllHostNames())
			.connectTimeout(Duration.ofSeconds(20))
            .build();
	}
}
