package sensor2graph.georev;

import org.w3c.dom.*;
import sensor2graph.*;

import java.util.*;
import java.util.stream.*;

public class Geodata {
	public String country_code, country, state, city, town, village;

	private Geodata(String country_code, String country, String state, String city, String town, String village) {
		this.country_code = country_code;
		this.country = country;
		this.state = state;
		this.city = city;
		this.town = town;
		this.village = village;
	}

	static Geodata fromXML(Stream<Document> xml_geo_zooms) {
		Map<String, String> tags = xml_geo_zooms
			.flatMap(zoom -> Util.xml_children_elems(zoom, "addressparts"))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a))
		;

		return new Geodata(
			tags.get("country_code"),
			tags.get("country"),
			tags.get("state"),
			tags.get("city"),
			tags.get("town"),
			tags.get("village")
		);
	}
}