package sensor2graph.georev;

class Coordinates {
	public Coordinates(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double latitude, longitude;

	@Override
	public int hashCode() {
		return (latitude + "" + longitude).hashCode();
	}

	@Override
	public boolean equals(Object other) {
		Coordinates c = (Coordinates) other;
		return latitude == c.latitude && longitude == c.longitude;
	}
	
	@Override
	public String toString() {
		return "coordinates(" + latitude + ", " + longitude + ")";
	}
}