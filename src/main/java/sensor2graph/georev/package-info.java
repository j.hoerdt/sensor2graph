

/**
 * This package provides light wrapper over a reverse geocoding api like https://nominatim.org/release-docs/develop/api/Reverse/.
 * That means it offers the functionality of getting geoinformation like the country, province, city to specific coordinates.
 */
package sensor2graph.georev;