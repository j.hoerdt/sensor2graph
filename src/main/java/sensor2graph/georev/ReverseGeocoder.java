package sensor2graph.georev;

import org.w3c.dom.*;
import sensor2graph.*;

import javax.xml.parsers.*;
import java.io.*;
import java.net.*;
import java.util.concurrent.*;
import java.util.logging.*;
import java.util.stream.*;

public class ReverseGeocoder {
	private static ReverseGeocoder inst;
	static {
		inst = new ReverseGeocoder("https://nominatim.openstreetmap.org/", 2001);
	}

	public static ReverseGeocoder osm_instance() {
		return inst;
	}

	private long request_timeout_millis;
	private String nominatim_provider_uri;
	private ConcurrentMap<Coordinates, Geodata> ram_cache = new ConcurrentHashMap<>();
	private long last_request_milli_time_point = 0;

	private ReverseGeocoder(String nominatim_provider_uri, long request_timeout_millis) {
		this.request_timeout_millis = request_timeout_millis;
		this.nominatim_provider_uri = nominatim_provider_uri;
	}

	public Geodata reverse_geocode(double latitude, double longitude) throws Exception {
		return ram_cached_request(new Coordinates(latitude, longitude));
	}

	private Geodata ram_cached_request(Coordinates coordinates) throws Exception {
		Geodata geodata = ram_cache.get(coordinates);
		if (geodata == null) {
			Main.glogger.log(Level.FINER, coordinates + " not in ram cache, looking in rom cache");
			geodata = rom_cached_coord_request(coordinates.latitude, coordinates.longitude);
			ram_cache.put(coordinates, geodata);
		}

		return geodata;
	}

	private Geodata rom_cached_coord_request(double latitude, double longitude) throws Exception {
		DocumentBuilder document_builder = Util.create_quiet_document_builder();

		String zoom14_query = make_query_string(latitude, longitude, 14);
		String zoom10_query = make_query_string(latitude, longitude, 10);

		Geodata result = Geodata.fromXML(Stream.of(
			rom_cached_request(zoom14_query, document_builder),
			rom_cached_request(zoom10_query, document_builder)
		));

		if (result.city         == null) Main.glogger.log(Level.WARNING, "could not determine city from queries "         + zoom10_query + " and " + zoom14_query + ", using null instead");
		if (result.state        == null) Main.glogger.log(Level.WARNING, "could not determine state from queries "        + zoom10_query + " and " + zoom14_query + ", using null instead");
		if (result.country      == null) Main.glogger.log(Level.WARNING, "could not determine country from queries "      + zoom10_query + " and " + zoom14_query + ", using null instead");
		if (result.country_code == null) Main.glogger.log(Level.WARNING, "could not determine country_code from queries " + zoom10_query + " and " + zoom14_query + ", using null instead");
		return result;
	}

	private Document rom_cached_request(String query_params, DocumentBuilder document_builder) throws Exception {
		File xml_geodata_file = new File(Main.config.getProperty("geocoding_cache"), query_params + ".xml");
		Document xml_geodata;

		if (xml_geodata_file.exists()) {
			try {
				xml_geodata = document_builder.parse(xml_geodata_file);
				Main.glogger.fine("found " + xml_geodata_file + " in rom cache");
			} catch (Exception e) {
				Main.glogger.log(Level.INFO, "parsing existing xml failed for " + xml_geodata_file + ", try requesting a fresh one.");
				try {
					uncached_request(query_params, new FileOutputStream(xml_geodata_file));
					xml_geodata = document_builder.parse(xml_geodata_file);
				} catch(Exception e1) {
					Main.glogger.log(Level.WARNING, "parsing or getting response from server failed", e1);
					throw e1;
				}
			}
		} else {
			Main.glogger.log(Level.FINE, "query " + query_params + " not in rom cache, making request to nominatim server");
			xml_geodata_file.getParentFile().mkdirs();
			uncached_request(query_params, new FileOutputStream(xml_geodata_file));
			xml_geodata = document_builder.parse(xml_geodata_file);
		}

		return xml_geodata;
	}

	private synchronized void uncached_request(String query_params, OutputStream response_output) throws Exception {
		Util.sleep_until(last_request_milli_time_point + request_timeout_millis);
		last_request_milli_time_point = System.currentTimeMillis();
		Main.glogger.log(Level.INFO, "Requesting query " + query_params + " from osm server at time " + last_request_milli_time_point + "ms");
		uncached_unlimited_request(query_params, response_output);
	}

	private void uncached_unlimited_request(String query_params, OutputStream response_output) throws IOException {
		URLConnection connection = Util.create_timed_connection(nominatim_provider_uri + "reverse?email=j.hoerdt@stud.uni-goettingen.de&" + query_params);
		
		Main.glogger.fine("opening uncached request stream to nominatim server for query " + query_params);
		connection.getInputStream().transferTo(response_output);
		Main.glogger.fine("closing successful connection to nominatim server for query " + query_params);
	}

	private static String make_query_string(double latitude, double longitude, int zoom) {
		return "accept-language=en&zoom=" + zoom + "&lat=" + latitude + "&lon=" + longitude;
	}
}