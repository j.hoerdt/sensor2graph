package sensor2graph;

import org.w3c.dom.Node;
import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.*;
import javax.xml.parsers.*;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.time.*;
import java.time.format.*;
import java.util.*;
import java.util.stream.*;

public class Util {
	private Util() {}

	private static int timeout_millis = 120000;

	public static URLConnection create_timed_connection(String uri) throws IOException {
		URLConnection connection = URI.create(uri).toURL().openConnection();
		connection.setConnectTimeout(timeout_millis);
		connection.setReadTimeout(timeout_millis);
		return connection;
	}

	public static BufferedReader reader_of_uri(String uri) throws IOException {
		return new BufferedReader(new InputStreamReader(create_timed_connection(uri).getInputStream()));
	}

	public static void sleep_until(long milli_time_point) throws InterruptedException {
		Thread.sleep(Math.max(0, milli_time_point - System.currentTimeMillis()));
	}

	public static DocumentBuilder create_quiet_document_builder() throws ParserConfigurationException {
		var document_builder_factory = DocumentBuilderFactory.newInstance();
		// ensure that no external entities can be loaded, see https://rules.sonarsource.com/java/RSPEC-2755
		// OSM might try to pwn us, who knows.
		document_builder_factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		document_builder_factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
		document_builder_factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
		document_builder_factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		document_builder_factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

		var document_builder = document_builder_factory.newDocumentBuilder();
		document_builder.setErrorHandler(new ErrorHandler() {
			@Override
			public void warning(SAXParseException arg0) {}
		
			@Override
			public void fatalError(SAXParseException e) throws SAXException {
				throw e;
			}
		
			@Override
			public void error(SAXParseException e) throws SAXException {
				throw e;
			}
		});
		return document_builder;
	}

	public static Stream<Map.Entry<String, String>> xml_children_elems(Document doc, String tag) {
		Node parent = doc.getElementsByTagName(tag).item(0);
		if (parent == null) return Stream.empty();
		NodeList children = parent.getChildNodes();

		return IntStream.range(0, children.getLength())
			.mapToObj(children::item)
			.map(node -> Map.entry(node.getNodeName(), node.getTextContent()))
		;
	}

	public static void append_to_file(String fname, String line) throws IOException {
		if (new File(fname).createNewFile()) {
			Main.glogger.info("created missing file "+ fname);
		}
		Files.writeString(Path.of(fname), line + '\n', StandardOpenOption.APPEND);
	}

	public static Instant parseAnyDateTime(String str) {
		// formatters for times directly convertible to Instant
		for (var formatter : List.of(
			DateTimeFormatter.ISO_INSTANT,
			DateTimeFormatter.ISO_OFFSET_DATE_TIME
		)) {
			try {
				return formatter.parse(str, Instant::from);
			} catch (DateTimeParseException ex) {}
		}

		// formatters for local times assumed to be UTC
		try {
			return Instant.from(
				DateTimeFormatter.ISO_LOCAL_DATE_TIME.parse(str, LocalDateTime::from)
					.atOffset(ZoneOffset.UTC)
			);
		} catch (DateTimeParseException ex) {}


		// safe until 2262
		var as_long = Long.parseLong(str);
		switch (str.length()) {
			case 10:
			return Instant.ofEpochSecond(as_long);

			case 13:
			return Instant.ofEpochMilli(as_long);

			case 16:
			return Instant.ofEpochSecond(0, as_long * 1000);

			case 19:
			return Instant.ofEpochSecond(0, as_long);

			default:
			throw new DateTimeException("what kind of timestamp is this supposed to be?: " + str);
		}
	}
}