package sensor2graph;

import com.google.gson.*;
import com.google.gson.reflect.*;
import com.influxdb.client.*;
import okhttp3.*;
import org.neo4j.driver.Logging;
import org.neo4j.driver.*;
import sensor2graph.webdirget.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.logging.*;
import java.util.stream.*;

public class Main {
	public static Properties config;
	
	public static Driver driver;
	public static InfluxDBClient influxDBClient;
	public static WriteApi influxdb_write_api;
	public static String handle_registry_session_id;
	public static java.util.logging.Logger glogger;
	private static java.util.logging.Logger logger;
	static {
		glogger = java.util.logging.Logger.getLogger("global");
		logger = java.util.logging.Logger.getLogger(Main.class.getName());
	}

	private static void initialize_neo4j_driver() {
		driver = GraphDatabase.driver(
			config.getProperty("neo4j_uri"),
			AuthTokens.basic(System.getenv("SENSOR2GRAPH_GRAPHDB_USER"), System.getenv("SENSOR2GRAPH_GRAPHDB_PASS")),
			Config.builder()
				.withLogging(Logging.console(Level.INFO))
				//.withConnectionAcquisitionTimeout(20, TimeUnit.SECONDS)
				//.withConnectionTimeout(10, TimeUnit.SECONDS)
				.withMaxConnectionLifetime(30, TimeUnit.SECONDS)
				//.withMaxTransactionRetryTime(3, TimeUnit.SECONDS)
				//.withLeakedSessionsLogging()
				.withMaxConnectionPoolSize(10)
				// .withEncryption()
			.build()
		);
	}

	private static void initialize_influxdb_driver() {
		// inspired by https://github.com/influxdata/influxdb-client-java/blob/86bda85ca6f0acf5bdea9883645f47e93d501003/client/src/main/java/com/influxdb/client/InfluxDBClientFactory.java#L173 with added timeout
		influxDBClient = InfluxDBClientFactory.create(
			InfluxDBClientOptions.builder()
                .url(Main.config.getProperty("influxdb_uri"))
                .authenticateToken(String.format("%s:%s",
					System.getenv("SENSOR2GRAPH_INFLUXDB_USER"),
					System.getenv("SENSOR2GRAPH_INFLUXDB_PASS")
				).toCharArray())
                .bucket(Main.config.getProperty("influxdb_database_name"))
                .org("-")
				.okHttpClient(
					new OkHttpClient().newBuilder()
						.connectTimeout(10, TimeUnit.MINUTES)
						.readTimeout(10, TimeUnit.MINUTES)
						.writeTimeout(10, TimeUnit.MINUTES)
				)
                .build()
		).enableGzip();

		influxdb_write_api = influxDBClient.makeWriteApi();
	}

	private static Set<String> parse_line_separated_values_file(String fname) throws Exception {
		File file = new File(fname);
		if (file.createNewFile()) {
			Main.logger.info(fname + " did not exist and was created.");
		}

		try (
			BufferedReader reader = new BufferedReader(new FileReader(file));
		) {
			return reader.lines().collect(Collectors.toSet());
		}
	}

	private static void run() throws Exception {
		try_uploading_failed_sensors();
		upload_all_days();
		try_uploading_failed_sensors();
	}

	private static void try_uploading_failed_sensors() throws Exception {
		Main.logger.info("Trying again to upload failed csv files in case of transient errors");
		Set<String> failed_csv_uris = parse_line_separated_values_file(config.getProperty("failed_to_upload_sensors_file"));
		Path backup = Path.of(config.getProperty("failed_to_upload_sensors_file") + ".backup");

		//those who fail again will get rewritten
		try {
			Files.move(Path.of(config.getProperty("failed_to_upload_sensors_file")), backup);
		} catch (FileAlreadyExistsException e) {
			Main.logger.warning(backup + " already existed. Looks like the program was abruptly terminated last time. Continuing with backup.");
			Files.move(backup, Path.of(config.getProperty("failed_to_upload_sensors_file")), StandardCopyOption.REPLACE_EXISTING);
			failed_csv_uris = parse_line_separated_values_file(config.getProperty("failed_to_upload_sensors_file"));
		}

		Main.logger.fine("failed csv files:\n" + failed_csv_uris.stream().collect(Collectors.joining("\n")));
		try {
			DayUploader.upload_from_csv_uris(failed_csv_uris.stream());
		} catch (Exception e) {
			Main.logger.log(Level.SEVERE, "uploading failed csvfiles failed fatally, restoring " + config.getProperty("failed_to_upload_sensors_file"), e);
			Files.move(backup, Path.of(config.getProperty("failed_to_upload_sensors_file")), StandardCopyOption.REPLACE_EXISTING);
			throw e;
		}

		if (!Files.deleteIfExists(backup)) {
			Main.logger.warning(backup + " somehow did not exist anymore");
		}
		
		Main.logger.info("Finished trying to upload failed csv files");		
	}


	private static void upload_all_days() throws Exception {
		Main.logger.info("Start to upload all sensors");

		Set<String> already_uploaded_day_uris = parse_line_separated_values_file(config.getProperty("already_uploaded_days_file"));

		Stream<String> daily_folders;
		try {
			daily_folders = DirectoryIndexer.get_daily_folders_in_html(Util.reader_of_uri(Main.config.getProperty("archive_uri")), Main.config.getProperty("archive_uri"));
		} catch (Exception e) {
			throw new Exception("could not read list of folders from " + Main.config.getProperty("archive_uri"), e);
		}

		daily_folders
			.filter(Predicate.not(already_uploaded_day_uris::contains))
			.forEach(folder_uri -> {
				try {
					Main.logger.info("Starting to upload sensors from day: " + folder_uri);
					DayUploader.upload_daily_folder(folder_uri);
					Main.logger.info("Finished uploading sensors from day: " + folder_uri);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			})
		;

		Main.logger.info("Finished uploading all sensors");
	}


	public static void main(String[] args) { 
		Main.logger.fine("Commandline arguments are: " + Stream.of(args).collect(Collectors.joining(", ")));
		String config_file = null;
		try {
			config_file = parse_args(args);
		} catch (Exception e) {
			Main.logger.log(Level.SEVERE, "Usage: <this_program> [config_file]", e);
			System.exit(-1);
		}

		Main.logger.fine("parsing config file");
		try {
			config = load_user_defined_properties_with_defaults(config_file, get_default_properties());
		} catch (Exception e) {
			Main.logger.log(Level.SEVERE, "Failed parsing the configuration file.", e);
			System.exit(-1);
		}
		
		{
			var http_agent = "Please/0.5";
			Main.logger.fine("setting http agent to: " + http_agent);
			System.setProperty("http.agent", http_agent);
		}

		Main.logger.fine("initializing neo4j driver.");
		initialize_neo4j_driver();
		try {
			Main.logger.fine("initializing influxdb driver.");
			initialize_influxdb_driver();
			try {
				try {
					Handle.http_client = Handle.get_client();
					test_auth();
					pre_run_graphdb_actions();
					run();
				} catch (org.neo4j.driver.exceptions.AuthenticationException e) {
					Main.logger.log(Level.SEVERE, "Authentication failed, are your credentials correct?");
				} catch (Exception e) {
					Main.logger.log(Level.SEVERE, "An unrecoverable exception occured, terminating...", e);
				}
			} finally {
				influxdb_write_api.close();
				influxDBClient.close();
			}
		} finally {
			driver.close();
		}
	}


	private static Properties load_user_defined_properties_with_defaults(String fname, Properties defaults) throws Exception {
		var config = new Properties(defaults);
		try (var file = new FileInputStream(fname)) {
			config.load(file);
		}
		return config;
	}


	private static Properties get_default_properties() {
		Properties defaults = new Properties();
		defaults.setProperty("already_uploaded_days_file",    "already_uploaded_days.txt"                 );
		defaults.setProperty("failed_to_upload_sensors_file", "failed_to_upload.txt"                      );
		defaults.setProperty("database_name",                 "neo4j"                                     );
		defaults.setProperty("csv_directory",                 "data/csv_files/"                           );
		defaults.setProperty("geocoding_cache",               "data/geocoding_cache/"                     );
		defaults.setProperty("archive_uri",                   "https://archive.sensor.community/"         );
		defaults.setProperty("sensor_type_info",              "sensor_type_info.json"                     );
		defaults.setProperty("pid_registry_uri",              "https://vm13.pid.gwdg.de:8000/api/handles/");
		defaults.setProperty("prefix_for_new_pids",           "21.11138/"                                 );
		defaults.setProperty("pid_reg_server_cert",           "pid_reg_server_cert.pem"                   );
		defaults.setProperty("pid_reg_keystore",              "pid_reg_keystore.p12"                      );
		defaults.setProperty("influxdb_database_name",        "openforecast"                              );
		
		return defaults;
	}


	private static String parse_args(String[] args) throws Exception {
		Main.logger.fine("checking for correct arg count");

		if (args.length == 0) {
			return "sensor2graph.properties";
		}

		if (args.length == 1) {
			return args[0];
		}

		throw new Exception("bad CLI arg count");
	}


	private static void pre_run_graphdb_actions() throws Exception {
		Map<String, Object> parameters = new Gson().fromJson(new FileReader(Main.config.getProperty("sensor_type_info")), new TypeToken <Map<String, Object>>() {}.getType());
		try (var session = Main.driver.session(SessionConfig.forDatabase(Main.config.getProperty("database_name")))) {
			session.writeTransaction(tx -> {
				tx.run("""
					unwind $sensor_type_info as sensor_type_info
					merge (n:SensorType {sensor_type: sensor_type_info.props.sensor_type})
					set n = sensor_type_info.props
					with n, sensor_type_info
					unwind sensor_type_info.measured_variables as measured_variable
					merge (m:MeasuredVariable {variable: measured_variable})
					merge (n)-[:MEASURES]->(m)""",
					parameters
				);
				return 42;
			});
		}
	}


	private static void test_auth() {
		Main.logger.fine("testing neo4j authentication");
		try (Session session = Main.driver.session(SessionConfig.forDatabase(Main.config.getProperty("database_name")))) {
			session.readTransaction(tx -> tx.run(new Query("match (n) return n limit 3")));
		}
	}
}