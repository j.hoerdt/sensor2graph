package sensor2graph.webdirget;

import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

/**
 * This class offers functions to search html pages for links
 */
public class DirectoryIndexer {
	private DirectoryIndexer() {}

	private static Stream<String> get_relative_subpaths_of_html(BufferedReader html) throws IOException {
		List<String> html_index_lines;
		try (BufferedReader reader = html) {
			html_index_lines = reader.lines().toList();
		}
		return get_relative_subpaths_of_html(html_index_lines.stream());
	}

	private static Stream<String> get_relative_subpaths_of_html(Stream<String> html_index_lines) {
		Pattern link_pattern = Pattern.compile("href=\"([^\"]+)\"");
		return html_index_lines.flatMap(line -> {
			Matcher m = link_pattern.matcher(line);
			if (m.find()) {
				return Stream.of(m.group(1));
			} else {
				return Stream.empty();
			}
		});
	}

	private static Stream<String> get_subpaths_of_html(BufferedReader html, String page_uri) throws IOException {
		return get_relative_subpaths_of_html(html).map(path -> page_uri + path);
	}

	private static Predicate<String> is_csv_file = Pattern.compile(".+\\.csv\\z").asMatchPredicate();
	private static Predicate<String> is_daily_folder = Pattern.compile(".+\\d{4}-\\d{2}-\\d{2}/\\z").asMatchPredicate();

	public static Stream<String> get_csv_files_in_html(BufferedReader html, String page_uri) throws IOException {
		return get_subpaths_of_html(html, page_uri).filter(is_csv_file);
	}

	public static Stream<String> get_daily_folders_in_html(BufferedReader html, String page_uri) throws IOException {
		return get_subpaths_of_html(html, page_uri).filter(is_daily_folder);
	}
}