package sensor2graph;

import com.influxdb.client.domain.*;
import com.influxdb.client.write.*;
import org.neo4j.driver.*;
import sensor2graph.webdirget.*;

import java.io.File;
import java.io.*;
import java.net.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.*;
import java.util.stream.*;

class DayUploader {
	private DayUploader() {}
	private static ForkJoinPool thread_pool = new ForkJoinPool(10);
	private static TransactionConfig transaction_config = TransactionConfig.builder().withTimeout(Duration.ofMinutes(1)).build();
	private static FileAttribute<?> daily_folder_perms = PosixFilePermissions.asFileAttribute(PosixFilePermissions.fromString("rwxr-xr-x"));

	public static void upload_from_csv_uris(Stream<String> csv_file_uris) throws Exception {
		ForkJoinTask<?> task = thread_pool.submit(make_uploading_task(csv_file_uris.parallel().unordered()));
		task.get(7, TimeUnit.HOURS);
	}

	private static Runnable make_uploading_task(Stream<String> csv_file_uris) {
		return () -> {
			Main.glogger.fine("creating result stream");

			Stream<Integer> results = csv_file_uris.flatMap(file_uri -> {
				Main.glogger.fine("creating session in thread " + Thread.currentThread().getName());

				try {
					var saved_csv_file = new File(Main.config.getProperty("csv_directory"), new URL(file_uri).getFile());
					download_csv_to_disk(saved_csv_file, file_uri);

					
					// init sensor after saving to get broken csvs also
					var sensor = make_sensor_from_csv(saved_csv_file, file_uri);

					put_data_into_influxdb(saved_csv_file, sensor);
					
					Main.glogger.info("uploading sensor " + file_uri);
					try (var session = Main.driver.session(SessionConfig.forDatabase(Main.config.getProperty("database_name")))) {
						sensor.process(session, transaction_config);
					}
					Main.glogger.fine("uploaded sensor " + file_uri);
				// exceptions that indicate errors not specific to individual sensors
				} catch (java.io.IOException | java.util.concurrent.RejectedExecutionException
						| org.neo4j.driver.exceptions.DatabaseException | com.influxdb.exceptions.InfluxException e) {
					throw new RuntimeException(e);
				// exceptions that are specific to individual sensors
				} catch (Exception e) {
					Main.glogger.log(Level.SEVERE, "sensor " + file_uri + " not uploaded because: " + e.getMessage(), e);
					try {
						Util.append_to_file(Main.config.getProperty("failed_to_upload_sensors_file"), file_uri);
					} catch (Exception e1) {
						Main.glogger.severe("Could not write to failed sensors file " + Main.config.getProperty("failed_to_upload_sensors_file") + " because: " + e1.getMessage());
						throw new RuntimeException(e1);
					}
					return Stream.empty();
				}

				Main.glogger.fine("closed session");
				return Stream.of(1);
			});
			Main.glogger.fine("result stream complete, counting results");

			//guarantee to evaluate entire stream
			results.forEach(res -> {});

			Main.glogger.info("finished uploading");
		};
	}

	private static void put_data_into_influxdb(File saved_csv_file, Sensor sensor) throws IOException {
		var tag_columns = List.of("sensor_id", "sensor_type", "location");
		var example_parsing_exception = new Object(){ NumberFormatException e = null; };

		try (var csv = new BufferedReader(new FileReader(saved_csv_file))) {
			var index_of_field = get_index_of_field_map(csv.readLine());
			var field_columns = new HashSet<>(index_of_field.keySet());
				field_columns.remove("timestamp");
				field_columns.removeAll(tag_columns);

			csv.lines()
				.map(line -> line.split(";", -1))
				.forEach(data -> {
					var point = Point.measurement("sensor")
						.time(Util.parseAnyDateTime(data[index_of_field.get("timestamp")]), WritePrecision.NS);

					for (var tag : tag_columns) {
						point.addTag(tag, (String) sensor.properties.get(tag));
					}
					for (var field : field_columns) {
						try {
							point.addField(field, Double.parseDouble(data[index_of_field.get(field)]));
						} catch (NumberFormatException e) {
							example_parsing_exception.e = e;
						}
					}

					Main.influxdb_write_api.writePoint(point);
				}
			);
		}
		
		if (example_parsing_exception.e != null) {
			Main.glogger.warning("there were lines with columns where I could not parse double from string due to for example " + example_parsing_exception.e);
		}

		Main.influxdb_write_api.flush();
	}

	private static Map<String, Integer> get_index_of_field_map(String header_line) {
		var header = header_line.split(";", -1);
		var index_of_field = new HashMap<String, Integer>();
		for (int i = 0; i < header.length; ++i) {
			index_of_field.put(header[i], i);
		}
		return index_of_field;
	}

	private static Sensor make_sensor_from_csv(File saved_csv_file, String file_uri) throws Exception {
		try (var csv = new BufferedReader(new FileReader(saved_csv_file))) {
			return Sensor.fromCSV(file_uri, read_one_csv_line(csv));
		}
	}

	private static void download_csv_to_disk(File saved_csv_file, String file_uri) throws IOException {
		Files.createDirectories(saved_csv_file.getParentFile().toPath(), daily_folder_perms);
		Main.glogger.info("copying csv to " + saved_csv_file);
		try (
			FileWriter csv_file_writer = new FileWriter(saved_csv_file);
			BufferedReader csv = Util.reader_of_uri(file_uri)
		) {
			csv.transferTo(csv_file_writer);
		}
	}
	

	private static Map<String, String> read_one_csv_line(BufferedReader csv_reader) throws IOException {
		Iterator<String> header = Arrays.asList(csv_reader.readLine().split(";")).iterator();
		Iterator<String> data   = Arrays.asList(csv_reader.readLine().split(";")).iterator();
		var result = new HashMap<String, String>();
		
		while (header.hasNext() && data.hasNext()) {
			result.put(header.next(), data.next());
		}

		return result;
	}
	

	private static Stream<String> get_csv_files(BufferedReader html, String csv_folder_uri) throws Exception {
		try {
			return DirectoryIndexer.get_csv_files_in_html(html, csv_folder_uri);
		} catch (Exception e) {
			throw new Exception("failed getting list of csv files for " + csv_folder_uri, e);
		}
	}

	private static Stream<String> get_csv_files(String csv_folder_uri) throws Exception {
		return get_csv_files(Util.reader_of_uri(csv_folder_uri), csv_folder_uri);
	}

	public static void upload_daily_folder(String daily_folder_url) throws Exception {
		try {
			upload_from_csv_uris(get_csv_files(daily_folder_url));
		} catch (Exception e) {
			Main.glogger.log(Level.SEVERE, "could not import " + daily_folder_url + ", not adding to uploaded folders", e);
			return;
		}
		create_daily_archive(new URL(daily_folder_url));
		remove_daily_folder(new URL(daily_folder_url));

		Main.glogger.fine("adding " + daily_folder_url + " to list of successful days");
		Util.append_to_file(Main.config.getProperty("already_uploaded_days_file"), daily_folder_url);
	}

	public static void create_daily_archive(URL daily_folder_url) throws Exception {
		String date = daily_folder_url.getFile().substring(1, 11);
		String tarname = date + ".tar.zst";

		Main.glogger.info("archiving " + date + " to " + tarname);

		Files.createDirectories(new File(Main.config.getProperty("csv_directory"), date).toPath(), daily_folder_perms);

		var tar_zstd = new ProcessBuilder("tar", "--zstd", "-cf", tarname, date)
			.directory(new File(Main.config.getProperty("csv_directory")))
			.redirectErrorStream(true);

		run_process(tar_zstd, "tar");
	}

	public static void remove_daily_folder(URL daily_folder_url) throws Exception {
		String date = daily_folder_url.getFile().substring(1, 11);

		Main.glogger.info("deleting archived folder " + date);

		//easier than stdlib:
		var rm = new ProcessBuilder("rm", "-rf", date)
			.directory(new File(Main.config.getProperty("csv_directory")))
			.redirectErrorStream(true);

		run_process(rm, "rm");
	}

	public static void run_process(ProcessBuilder process_builder, String name) throws Exception {
		long start_time = System.currentTimeMillis();
		var p = process_builder.start();

		if (!p.waitFor(120, TimeUnit.MINUTES) /*waiting time elapsed*/) {
			throw new TimeoutException(name + " process took too long");
		}
		
		Main.glogger.info(name + " took " + (System.currentTimeMillis() - start_time) + "ms to complete.");

		if (p.exitValue() != 0) {
			Main.glogger.warning(name + " failed, output:");
			try (BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()))) {
				Main.glogger.warning("\n" + r.lines().collect(Collectors.joining("\n")));
			}
			throw new Exception(name + " returned " + p.exitValue());
		}
	}
}