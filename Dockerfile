FROM docker.io/library/eclipse-temurin:17 as compile
WORKDIR /sensor2graph

# download gradle:
COPY gradle/ ./gradle
COPY gradlew settings.gradle .
RUN chmod +x gradlew && ./gradlew

# download dependencies, would be nice to have. https://github.com/gradle/gradle/issues/1049
#COPY build.gradle .
#RUN ./gradlew downloadDependencies

# produce fat jar
COPY . .
RUN chmod +x gradlew && ./gradlew jar

FROM docker.io/library/eclipse-temurin:17
RUN apt update && apt install -y tar zstd
WORKDIR /sensor2graph
RUN chmod -R o+w .
ENTRYPOINT ["bash", "-c", "umask 0007 && java -Xmx3g -Djava.util.logging.config.file=logging.properties -jar sensor2graph.jar"]
RUN mkdir logs
COPY --from=compile /sensor2graph/build/libs/sensor2graph.jar /sensor2graph/
COPY --from=compile /sensor2graph/config/logging.properties /sensor2graph/config/sensor2graph.properties /sensor2graph/config/sensor_type_info.json /sensor2graph/
